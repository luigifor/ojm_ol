window.onload = init;

function init() {
  const map = new ol.Map({
    target: "my-map",
    view: new ol.View({
      center: [1507296.1503846336, 5173753.926675993],
      zoom: 9,
    }),
  });

  const openStreetMapStandard = new ol.layer.Tile({
    source: new ol.source.OSM(),
    visible: true,
    title: "OSMStandard",
  });

  const stamenToner = new ol.layer.Tile({
    source: new ol.source.XYZ({
      url: "https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png",
      attributions:
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
    }),
    visible: false,
    title: "StamenToner",
  });

  const stamenTerrain = new ol.layer.Tile({
    source: new ol.source.XYZ({
      url: "https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg",
      attributions:
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
    }),
    visible: false,
    title: "StamenTerrain",
  });

  const gruppoLayerMappeBase = new ol.layer.Group({
    layers: [openStreetMapStandard, stamenTerrain, stamenToner],
  });

  map.addLayer(gruppoLayerMappeBase);

  const radioSwitcher = document.querySelectorAll("input[name=selettoreBase]");

  for (let radioInput of radioSwitcher) {
    radioInput.addEventListener("change", function () {
      let titoloMappaSelezionata = this.value;
      gruppoLayerMappeBase.getLayers().forEach((element) => {
        let titoloMappaTemp = element.get("title");

        element.setVisible(titoloMappaTemp === titoloMappaSelezionata);
      });
    });
  }
  // -------- LAYER

  const strokeStyle = new ol.style.Stroke({
    color: [20, 150, 75, 0.8],
    width: 3,
  });
  const strokeStyleUno = new ol.style.Stroke({
    color: [20, 150, 255, 0.8],
    width: 3,
  });
  const strokeStyleDue = new ol.style.Stroke({
    color: [20, 70, 70, 0.8],
    width: 3,
  });
  const strokeStyleTre = new ol.style.Stroke({
    color: [150, 0, 75, 0.8],
    width: 3,
  });

  const circleStyle = new ol.style.Circle({
    fill: new ol.style.Fill({
      color: [255, 73, 44, 0.5],
    }),
    stroke: strokeStyle,
    radius: 7,
  });

  const percorsiGeoJson = new ol.layer.VectorImage({
    source: new ol.source.Vector({
      url: "./vettori/percorsi.geojson",
      format: new ol.format.GeoJSON(),
    }),
    visible: true,
    title: "Personalizzato",
    style: new ol.style.Style({
      stroke: strokeStyle,
      image: circleStyle,
    }),
  });

  const regioniGeoJSON = new ol.layer.VectorImage({
    source: new ol.source.Vector({
      url: "./vettori/regioni.geojson",
      format: new ol.format.GeoJSON(),
    }),
    visible: true,
    title: "Regioni",
    style: new ol.style.Style({
      stroke: strokeStyleUno,
      image: circleStyle,
    }),
  });
  const percorsoUnoGeoJson = new ol.layer.VectorImage({
    source: new ol.source.Vector({
      url: "./vettori/percorso1.geojson",
      format: new ol.format.GeoJSON(),
    }),
    visible: false,
    title: "Percorso1",
    style: new ol.style.Style({
      stroke: strokeStyleUno,
      image: circleStyle,
    }),
  });

  const percorsoDueGeoJson = new ol.layer.VectorImage({
    source: new ol.source.Vector({
      url: "./vettori/percorso2.geojson",
      format: new ol.format.GeoJSON(),
    }),
    visible: false,
    title: "Percorso2",
    style: new ol.style.Style({
      stroke: strokeStyleDue,
      image: circleStyle,
    }),
  });

  const percorsoTreGeoJson = new ol.layer.VectorImage({
    source: new ol.source.Vector({
      url: "./vettori/percorso3.geojson",
      format: new ol.format.GeoJSON(),
    }),
    visible: false,
    title: "Percorso3",
    style: new ol.style.Style({
      stroke: strokeStyleTre,
      image: circleStyle,
    }),
  });

  const gruppoLayerGeoJSON = new ol.layer.Group({
    layers: [
      percorsiGeoJson,
      percorsoUnoGeoJson,
      percorsoDueGeoJson,
      percorsoTreGeoJson,
    ],
  });
  map.addLayer(gruppoLayerGeoJSON);
  map.addLayer(regioniGeoJSON);

  const radioSwitcherPerc = document.querySelectorAll(
    "input[name=selettorePerc]"
  );

  for (let radioInput of radioSwitcherPerc) {
    radioInput.addEventListener("change", function () {
      let titoloPercorsoSelezionata = this.value;
      gruppoLayerGeoJSON.getLayers().forEach((element) => {
        let titoloPercTemp = element.get("title");

        element.setVisible(titoloPercTemp === titoloPercorsoSelezionata);
      });
    });
  }

  // --------OVERLAY----
  const overlayHtml = document.querySelector(".overlay-container");
  const overlayLayer = new ol.Overlay({
    element: overlayHtml,
  });
  map.addOverlay(overlayLayer);

  const spanOverlayName = document.getElementById("overlay-name");
  const spanOverlayInfo = document.getElementById("overlay-info");

  map.on("click", function (evt) {
    overlayLayer.setPosition(undefined);
    map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
      let nome = feature.get("name");
      let info = feature.get("desc");

      spanOverlayInfo.innerHTML = info;
      spanOverlayName.innerHTML = nome;

      overlayLayer.setPosition(evt.coordinate);
    });
  });
}
